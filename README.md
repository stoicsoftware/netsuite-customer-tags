## Simple Customer Tags

This SuiteApp gives Sales and Marketing departments the ability to quickly and
easily tag Customers for the purpose of segmenting in NetSuite.

These tags are built on NetSuite's native Customer Groups functionality, so all 
existing Group functionality (e.g. sending campaign emails to Groups) is
retained by the tags as well.
